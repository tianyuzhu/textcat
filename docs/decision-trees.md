### The Decision Tree

- All nodes in the decision tree are word features.
- Two edges from each node to its children, representing "yes" or "no", whether or not the word feature appears in the article.

Thus the tree is a binary tree.

- The leaf nodes refer to the most likely category.
- The non-leaf nodes refer to a word feature with left and right children corresponding to whether the word feature appears in the article.
- A potiential-node is a 2-tuple (leaf-node, word-feature).
- The information gain of a potiential-node is the information gain of the word-feature at the leaf-node.
- At every iteration of tree-creation, we replace a leaf-node with a corresponding potential-node with the greatest information gain.
  - We remove the other nodes corresponding to the chosen leaf node from consideration.
  - We then generate the left and right partitions of the node and add the corresponding potential nodes into consideration.


### Algorithm for determining adding the next node to the tree.

    add-next-node
    	potential <= considerations.first
    	node <= potential.leaf-node
    	mid <= range.partition(potential.word-feature)
    	left-range <= range from start to mid
    	right-range <= range from mid to end
    	node.left <= leaf-node(left-range)
    	node.right <= leaf-node(right-range)
    	add-to-considerations(node.left)
    	add-to-considerations(node.right)

    add-to-considerations(leaf-node)
    	range <= node.range
    	max-word = word feature with max value
    	max-potential = potential-node(node, max-word)
    	considerations.insert(max-potential)

considerations: a heap sorted by max value.
value: information-gain or information-gain*num-documents
