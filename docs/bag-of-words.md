## How do you quickly determine that a word is in an article?

### Analysis:

- There are more than 3000 distinct word features.
- Each article contains less than 300 word-features each.
- All articles in data files appear in order of Article ID.
- All words in each article appear in order of Word ID.

### Implementation:

Represent each article as a vector of words it contains sorted by Word ID.

To determine if a word is in the article:

    containsWord(vector, word):
    	return std::binary_search on vector for word

Let `n = |vector|`, then:

- `O(log(n))` run time
- `O(n)` space
