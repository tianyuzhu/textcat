#ifndef NAIVE_BAYES_HPP
#define NAIVE_BAYES_HPP

#include <algorithm>
#include <functional>
#include <unordered_map>
#include <vector>
#include <cmath>

template<typename Data, typename Feature, typename Class>
class NaiveBayesNet
{
	typedef
	typename std::vector<Data>::iterator
	DataIt;

	typedef
	typename std::vector<Class>::iterator
	ClassIt;

	template<typename Key, typename Value>
	using Map = std::unordered_map<Key, Value>;

	typedef Map<Class, double> Factor;

public:
	template<
		typename FeaturesOfFn,
		typename FeatureInFn,
		typename ClassOfFn>
	NaiveBayesNet(
		DataIt dataBegin, DataIt dataEnd,
		ClassIt classBegin, ClassIt classEnd,
		FeaturesOfFn &&featuresOf,
		FeatureInFn &&featureIn,
		ClassOfFn &&clazzOf) :
	classBegin(classBegin), classEnd(classEnd),
	isFeatureIn(std::forward<FeatureInFn>(featureIn))
	{
		// Instantiate utility functions
		std::function<std::vector<Feature>(Data const&)>
		featOf(std::forward<FeaturesOfFn>(featuresOf));

		std::function<Class(Data const&)>
		classOf(std::forward<ClassOfFn>(clazzOf));

		Map<Class, unsigned> classes;
		factors.reserve(std::distance(dataBegin, dataEnd));

		// Tally up the data
		for (auto data=dataBegin; data!=dataEnd; ++data) {
			auto clazz = classOf(*data);
			++classes[clazz];
			for (auto feature : featOf(*data)) {
				++factors[feature][clazz];
			}
		}

		// Add Laplace smoothing for totals
		auto numClasses=std::distance(classBegin, classEnd);
		for (auto classIt = classBegin;
			classIt != classEnd;
			++classIt)
		{
			classes[*classIt] += numClasses;
		}

		// Calculate probabilities
		for (auto &fpair : factors) {
			auto &factor = fpair.second;
			for (auto classIt = classBegin;
				classIt != classEnd;
				++classIt)
			{
				auto &prob = factor[*classIt];
				prob += 1; // Laplace smoothing for values
				prob /= classes[*classIt];
			}
		}
	}

	Class decide(Data const& datum) const
	{
		auto classIt = classBegin;
		double maxProb = probOf(datum, *classIt);
		Class maxClass = *classIt;

		while (++classIt != classEnd) {
			double prob = probOf(datum, *classIt);
			if (prob <= maxProb) continue;
			maxProb = prob;
			maxClass = *classIt;
		}

		return maxClass;
	}

	std::vector<Feature> topFeatures(size_t n)
	{
		if (n > factors.size()) n = factors.size();

		typedef std::pair<double, Feature> Pair;
		std::vector<Pair> pairs;
		pairs.reserve(factors.size());
		for (auto &fpair : factors) {
			pairs.emplace_back(
				-discrimination(fpair.second),
				fpair.first);
		}

		std::partial_sort(
			pairs.begin(),
			pairs.begin() + n,
			pairs.end());

		std::vector<Feature> top;
		top.reserve(n);
		for (auto it=pairs.begin(), end=pairs.begin() + n;
			it != end;
			++it)
		{
			top.emplace_back(it->second);
		}

		return top;
	}

private:
	ClassIt classBegin, classEnd;
	std::function<bool(Data const&, Feature)> isFeatureIn;
	Map<Feature, Factor> factors;

	double probOf(Data const&datum, Class clazz) const
	{
		double prob = 1;
		for (auto &fpair : factors) {
			auto feature = fpair.first;
			auto &factor = fpair.second;
			prob *= probHasFeature(
				factor, clazz,
				isFeatureIn(datum, feature));
		}
		return prob;
	}

	double probHasFeature(
		Factor const&factor,
		Class givenClass,
		bool hasFeature) const
	{
		auto it = factor.find(givenClass);
		double p = (it == factor.end())? 0 : it->second;
		if (!hasFeature) p = 1-p;
		return p;
	}

	double discrimination(Factor const&factor)
	{
		auto it = factor.begin();
		double value = std::log2(it->second);
		while (++it != factor.end()) {
			value -= std::log2(it->second);
		}

		return std::abs(value);
	}
};

#endif // NAIVE_BAYES_HPP
