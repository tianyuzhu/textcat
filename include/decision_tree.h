#ifndef DECISION_TREE_H
#define DECISION_TREE_H

#include <algorithm>
#include <cmath>
#include <functional>
#include <iterator>
#include <unordered_map>
#include <utility>
#include <vector>

#include "internal/binary_tree.hpp"

enum Evaluation_Policy
{
	Information_Gain,
	Information_Gain_Times_Size
};

/**
 * A binary decision tree.
 */
template<typename Data, typename Feature, typename Class>
class DecisionTree
{
	typedef
	typename std::vector<Data>::iterator
	DataIt;

	typedef
	typename std::vector<Feature>::iterator
	FeatIt;

public:

	struct Decision;

	typedef internal::BinaryTree<Decision> Tree;
	typedef typename Tree::Node Node;

	struct Potential;

	/**
	 * Creates a new binary decision tree from some training
	 * data.
	 *
	 * The training data is a modifiable range of data
	 * that will be partitioned on the most valuable feature
	 * where value is a function of the answers.
	 */
	template<
		typename FeaturesOfData,
		typename FeatureInDatum,
		typename ClassOfDatum>
	DecisionTree(
		DataIt dataBegin, DataIt dataEnd,
		FeatIt featBegin, FeatIt featEnd,
		FeaturesOfData &&featuresOfData,
		FeatureInDatum &&featuresInDatum,
		ClassOfDatum &&classOfDatum,
		Evaluation_Policy evalPolicy = Information_Gain) :
	featBegin(featBegin), featEnd(featEnd),
	featsOf(std::forward<FeaturesOfData>(featuresOfData)),
	hasFeat(std::forward<FeatureInDatum>(featuresInDatum)),
	classOf(std::forward<ClassOfDatum>(classOfDatum)),
	evalPolicy(evalPolicy)
	{
		addRootNode(dataBegin, dataEnd);
	}

	void learnNode();

	Class decide(Data const&datum) const;

	const Node rootNode() const;

private:
	FeatIt featBegin, featEnd;

	std::function<
	std::vector<Feature> const&(Data const&)> featsOf;

	std::function<bool(Data const&, Feature)> hasFeat;
	std::function<Class(Data const&)> classOf;

	Evaluation_Policy evalPolicy;

	Tree tree;

	std::vector<Potential> potentials;

	void addRootNode(DataIt begin, DataIt end);
	void addLeftNode(Node parent, DataIt begin, DataIt end);
	void addRightNode(Node parent,DataIt begin, DataIt end);
	Class mostLikelyClass(DataIt begin, DataIt end);
	Feature maxValuedFeature(DataIt begin, DataIt end,
		double &valueOutput);

	template<typename T>
	using Tally = std::unordered_map<T, unsigned>;

	double entropy(unsigned total, Tally<Class> tally);

	double remainder(
		Feature feat,
		DataIt begin, DataIt end);

	double evaluate(
		Feature feat,
		double totalEntropy, unsigned dataSize,
		DataIt begin, DataIt end);

	template<typename Key>
	static
	bool tally_cmp(
		std::pair<Key, unsigned> const&p1,
		std::pair<Key, unsigned> const&p2)
	{
		return p1.second < p2.second;
	}
};

template<typename Data, typename Feature, typename Class>
struct DecisionTree<Data, Feature, Class>::Decision
{
	Decision(Class clazz,
		DataIt begin, DataIt end) :
	is_leaf(true),
	clazz(clazz),
	begin(begin),
	end(end)
	{}

	void branch(Feature feat, double val)
	{
		is_leaf = false;
		clazz.Class::~Class();
		begin.DataIt::~DataIt();
		end.DataIt::~DataIt();
		feature = feat;
		value = val;
	}

	bool is_leaf;
	union
	{
		struct // Not Leaf
		{
			Feature feature;
			double value;
		};
		struct // Leaf
		{
			Class clazz;
			DataIt begin, end;
		};
	};
};

template<typename Data, typename Feature, typename Class>
struct DecisionTree<Data, Feature, Class>::Potential
{
	Potential(double value, Feature feat, Node node) :
	value(value),
	feature(feat),
	node(node)
	{}

	bool operator <(Potential const&p) const
	{
		return value < p.value;
	}

	double value;
	Feature feature;
	Node node;
};

/// DecisionTree Implementations ///

template<typename Data, typename Feature, typename Class>
Class
DecisionTree<Data, Feature, Class>::
decide(Data const&datum) const
{
	Node node = tree.root();
	while (!node.data().is_leaf) {
		node = hasFeat(datum, node.data().feature)?
			node.left() : node.right();
	}
	return node.data().clazz;
}

template<typename Data, typename Feature, typename Class>
const typename DecisionTree<Data, Feature, Class>::Node
DecisionTree<Data, Feature, Class>::
rootNode() const
{
	return tree.root();
}

template<typename Data, typename Feature, typename Class>
void
DecisionTree<Data, Feature, Class>::
addRootNode(DataIt begin, DataIt end)
{
	Class clazz = mostLikelyClass(begin, end);
	double value;
	Feature feat = maxValuedFeature(begin, end, value);
	Node root = tree.emplaceRoot(clazz, begin, end);

	assert(potentials.empty() &&
		"potentials should be empty while inserting the"
		"root node!");

	potentials.emplace_back(value, feat, root);
}

template<typename Data, typename Feature, typename Class>
void
DecisionTree<Data, Feature, Class>::
addLeftNode(Node parent, DataIt begin, DataIt end)
{
	if (begin == end) return;
	Class clazz = mostLikelyClass(begin, end);
	double value;
	Feature feat = maxValuedFeature(begin, end, value);
	Node child = parent.emplaceLeft(clazz, begin, end);
	if (value == 0) return;
	potentials.emplace_back(value, feat, child);
	std::push_heap(potentials.begin(), potentials.end());
}

template<typename Data, typename Feature, typename Class>
void
DecisionTree<Data, Feature, Class>::
addRightNode(Node parent, DataIt begin, DataIt end)
{
	if (begin == end) return;
	Class clazz = mostLikelyClass(begin, end);
	double value;
	Feature feat = maxValuedFeature(begin, end, value);
	Node child = parent.emplaceRight(clazz, begin, end);
	if (value == 0) return;
	potentials.emplace_back(value, feat, child);
	std::push_heap(potentials.begin(), potentials.end());
}

template<typename Data, typename Feature, typename Class>
Class
DecisionTree<Data, Feature, Class>::
mostLikelyClass(DataIt begin, DataIt end)
{
	// Create a tally of 0's
	Tally<Class> tally;

	// Tally the classes
	for (auto datait = begin; datait != end; ++datait) {
		++tally[classOf(*datait)];
	}

	// Find the max element iterator
	auto maxit = std::max_element(
		tally.begin(), tally.end(),
		tally_cmp<Class>);

	// Return the max class
	return maxit->first;
}

template<typename Data, typename Feature, typename Class>
Feature
DecisionTree<Data, Feature, Class>::
maxValuedFeature(DataIt begin, DataIt end, double& valueOut)
{
	assert(begin != end &&
		"Can't find the max-valued feature with no data!");

	// Find total entropy
	unsigned total = 0;
	Tally<Class> tally;
	for (DataIt dataIt = begin; dataIt != end; ++dataIt) {
		++tally[classOf(*dataIt)];
		++total;
	}
	double I = entropy(total, std::move(tally));

	// Find max feature
	auto featIt = featBegin;
	auto maxVal = evaluate(*featIt, I, total, begin, end);
	Feature maxFeat = *featIt;
	while (++featIt != featEnd) {
		auto val = evaluate(*featIt, I, total, begin, end);
		if (val <= maxVal) continue;
		maxVal = val;
		maxFeat = *featIt;
	}

	valueOut = maxVal;
	return maxFeat;
}

template<typename Data, typename Feature, typename Class>
void
DecisionTree<Data, Feature, Class>::
learnNode()
{
	if (potentials.empty()) return;

	// Find the highest potential
	std::pop_heap(potentials.begin(), potentials.end());
	Potential potential = potentials.back();
	potentials.pop_back();

	Node node = potential.node;
	Decision &decision = node.data();

	// Partition the data according to
	// the potential's feature.
	Feature feat = potential.feature;
	DataIt begin = decision.begin;
	DataIt end = decision.end;
	DataIt mid = std::partition(begin, end,
		std::bind(hasFeat, std::placeholders::_1, feat));

	// Branch the decision and set its children
	decision.branch(feat, potential.value);
	addLeftNode(node, begin, mid);
	addRightNode(node, mid, end);
}

template<typename Data, typename Feature, typename Class>
double
DecisionTree<Data, Feature, Class>::
entropy(unsigned total, Tally<Class> tally)
{
	double entropy = 0;
	for (auto &pair : tally) {
		if (pair.second == 0) continue;
		double p = double(pair.second) / double(total);
		entropy += p*log2(p);
	}
	return -entropy;
}

template<typename Data, typename Feature, typename Class>
double
DecisionTree<Data, Feature, Class>::
remainder(Feature feat, DataIt begin, DataIt end)
{
	unsigned total = 0, totalYes = 0, totalNo = 0;
	Tally<Class> tallyYes, tallyNo;
	for (auto dataIt = begin; dataIt != end; ++dataIt) {
		bool hasFeature = hasFeat(*dataIt, feat);
		auto &tally = hasFeature? tallyYes : tallyNo;
		++tally[classOf(*dataIt)];
		++(hasFeature? totalYes : totalNo);
		++total;
	}

	double R = 0;
	R += double(totalYes) *
	     entropy(totalYes, std::move(tallyYes));
	R += double(totalNo) *
	     entropy(totalNo, std::move(tallyNo));
	return R / double(total);
}

template<typename Data, typename Feature, typename Class>
double
DecisionTree<Data, Feature, Class>::
evaluate(
	Feature feat,
	double totalEntropy, unsigned dataSize,
	DataIt begin, DataIt end)
{
	double value = totalEntropy;
	value -= remainder(feat, begin, end);
	if (evalPolicy == Information_Gain_Times_Size) {
		value *= dataSize;
	}
	return value;
}

#endif // DECISION_TREE_H
