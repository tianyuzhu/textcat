#ifndef ARTICLE_H
#define ARTICLE_H

#include <utility>
#include <vector>
#include <functional>

/**
 * A word-feature that appears in Articles.
 *
 * The existence of a word-feature within an article may
 * correlate with the category of the article.
 */
struct Word
{
	Word();
	Word(unsigned int id);

	unsigned int id;

	bool operator ==(Word w) const;

	bool operator <(Word w) const;

	Word operator ++();
	Word operator ++(int);
};

template<>
struct std::hash<Word>
{
	typedef Word argument_type;

	typedef
	typename std::hash<unsigned int>::result_type
	result_type;

	result_type operator ()(argument_type) const;
};

/**
 * An Article as a "bag of words".
 *
 * Articles are:
 * - Default constructible
 * - Moveable
 * - NOT copyable
 * - Immutable
 */
class Article
{
public:

	/**
	 * The type of an Article's ID number.
	 *
	 * Only positive ID numbers are valid.
	 */
	typedef unsigned int Id;

	/**
	 * Creates an empty article.
	 */
	Article();

	/**
	 * Creates an article containing the given words.
	 *
	 * The words are required to be given in sorted order
	 * such that std::is_sorted returns true.
	 *
	 * @param id    the Id of the article. Must be positive.
	 * @param words the words in the article
	 *              ordered by increasing id.
	 */
	template<typename Words>
	explicit Article(Id id, Words &&words) :
	_id(id),
	_words(std::forward<Words>(words))
	{
		assert_validity();
	}

	// Moveable

	Article(Article &&);
	Article &operator =(Article &&);

	// Not Copyable

	Article(Article const&) = delete;
	Article &operator =(Article const&) = delete;

	/**
	 * Checks if this article contains the given word.
	 * @param  Word The word to check for
	 * @return true iff this article contains
	 *         the given word.
	 */
	bool contains(Word) const;

	/**
	 * @return The ID of this Article.
	 */
	Id id() const;

	/**
	 * @return The Words appearing in this Article.
	 */
	std::vector<Word> const&words() const;

private:
	void assert_validity() const;

	Id _id;
	std::vector<Word> _words;
};

/**
 * A Label for an Article represents
 * the category of the Article.
 */
typedef unsigned int Label;

/**
 * A mapping of Articles to their Labels.
 *
 * ArticleLabels are:
 * - NOT default constructible
 * - Moveable
 * - NOT copyable
 * - Immutable
 */
class ArticleLabels
{
public:

	/**
	 * Creates a mapping of Articles to their Labels.
	 *
	 * The mapping is given by a list of Labels, where if an
	 * Article has id I and Label L, then L is in position
	 * I-1 of the list.
	 *
	 * @param labels the mapping from Article to its Label.
	 */
	template<typename Labels>
	explicit ArticleLabels(Labels &&labels) :
	labels(std::forward<Labels>(labels))
	{}

	/**
	 * Determines the Label of the given Article.
	 *
	 * The Article must have a corresponding Label,
	 * otherwise, assertions will fail!
	 *
	 * @param Article The given Article.
	 * @return The Label of the given Article.
	 */
	Label labelOf(Article const&) const;

private:
	std::vector<Label> labels;
};

#endif
