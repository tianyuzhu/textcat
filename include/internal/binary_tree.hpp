#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <cassert>
#include <utility>
#include <vector>

namespace internal
{

template<typename T>
class BinaryTree
{
	struct internal_node;

	typedef
	typename std::vector<internal_node>::size_type
	size_type;

	struct internal_node
	{
		template<typename... Args>
		internal_node(Args&&...);

		size_type left, right;
		T data;
	};

public:
	class Node
	{
		friend class BinaryTree<T>;
	public:

		Node &operator =(Node const&node);

		T &data();
		T const& data() const;

		template<typename... Args>
		Node emplaceLeft(Args&&...);

		template<typename... Args>
		Node emplaceRight(Args&&...);

		Node left();

		const Node left() const;

		Node right();

		const Node right() const;

	private:
		Node(size_type index,
			BinaryTree<T> &tree);

		internal_node &internal();

		internal_node const&internal() const;

		size_type index;
		BinaryTree<T> &tree;
	};

	BinaryTree() = default;

	explicit BinaryTree(size_type reserve);

	BinaryTree(BinaryTree &&tree) = default;

	BinaryTree &operator =(BinaryTree &&tree) = default;

	Node root();

	const Node root() const;

	template<typename... Args>
	Node emplaceRoot(Args&&...);

private:
	template<typename... Args>
	Node emplaceNode(Args&&...);

	std::vector<internal_node> nodes;
};

/// BinaryTree ///

template<typename T>
BinaryTree<T>::
BinaryTree(size_type reserve) :
BinaryTree()
{
	nodes.reserve(reserve);
}

template<typename T>
typename BinaryTree<T>::Node
BinaryTree<T>::
root()
{
	assert(!nodes.empty() &&
		"Can't get root node if the tree is empty!");
	return Node(size_type(0), *this);
}

template<typename T>
const typename BinaryTree<T>::Node
BinaryTree<T>::
root() const
{
	assert(!nodes.empty() &&
		"Can't get root node if the tree is empty!");
	return Node(size_type(0),
		*const_cast<BinaryTree<T>*>(this));
}

template<typename T>
template<typename... Args>
typename BinaryTree<T>::Node
BinaryTree<T>::
emplaceRoot(Args&&... args)
{
	assert(nodes.empty() &&
		"Nodes must be empty to emplace a root!");
	return emplaceNode(std::forward<Args>(args)...);
}

template<typename T>
template<typename... Args>
typename BinaryTree<T>::Node
BinaryTree<T>::
emplaceNode(Args&&... args)
{
	size_type i = nodes.size();
	nodes.emplace_back(std::forward<Args>(args)...);
	return Node(i, *this);
}

/// internal_node ///

template<typename T>
template<typename... Args>
BinaryTree<T>::internal_node::
internal_node(Args&&... args) :
left(-1),
right(-1),
data(std::forward<Args>(args)...)
{}

/// Node ///

template<typename T>
typename BinaryTree<T>::Node &
BinaryTree<T>::Node::
operator =(Node const&node)
{
	assert(&tree == &node.tree &&
		"Can't assign nodes of different trees!");
	index = node.index;
	return *this;
}

template<typename T>
BinaryTree<T>::Node::
Node(size_type index,
	BinaryTree<T> &tree) :
index(index),
tree(tree)
{}

template<typename T>
typename BinaryTree<T>::internal_node &
BinaryTree<T>::Node::
internal()
{
	return tree.nodes[index];
}

template<typename T>
typename BinaryTree<T>::internal_node const&
BinaryTree<T>::Node::
internal() const
{
	return tree.nodes[index];
}

template<typename T>
T &
BinaryTree<T>::Node::
data()
{
	return internal().data;
}

template<typename T>
T const&
BinaryTree<T>::Node::
data() const
{
	return internal().data;
}

template<typename T>
template<typename... Args>
typename BinaryTree<T>::Node
BinaryTree<T>::Node::
emplaceLeft(Args&&... args)
{
	Node n(tree.emplaceNode(std::forward<Args>(args)...));
	internal().left = n.index;
	return n;
}

template<typename T>
template<typename... Args>
typename BinaryTree<T>::Node
BinaryTree<T>::Node::
emplaceRight(Args&&... args)
{
	Node n(tree.emplaceNode(std::forward<Args>(args)...));
	internal().right = n.index;
	return n;
}

template<typename T>
typename BinaryTree<T>::Node
BinaryTree<T>::Node::
left()
{
	return Node(internal().left, tree);
}

template<typename T>
const typename BinaryTree<T>::Node
BinaryTree<T>::Node::
left() const
{
	return Node(internal().left, tree);
}

template<typename T>
typename BinaryTree<T>::Node
BinaryTree<T>::Node::
right()
{
	return Node(internal().right, tree);
}

template<typename T>
const typename BinaryTree<T>::Node
BinaryTree<T>::Node::
right() const
{
	return Node(internal().right, tree);
}

} // namespace internal

#endif // BINARY_TREE_H
