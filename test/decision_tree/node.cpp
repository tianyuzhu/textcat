#include "gtest/gtest.h"

#include "decision_tree.h"

using namespace std;

typedef int Integer;
typedef int Prime;
typedef char Category;

static const
Integer nums[] = {2, 4, 6, 17, 9, 42, 130};
static const
Prime primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
static const
Category categories[] = {'A', 'B'};

static vector<Prime> fake; // FIXME!!!

static
vector<Prime> const&primeFactorsOf(Integer i)
{
	vector<Prime> primeFactors;
	for (auto p : primes) {
		if (i % p == 0)
			primeFactors.push_back(p);
	}
	fake = move(primeFactors);
	return fake;
}

static
bool isPrimeFactor(Prime p, Integer i)
{
	return i % p == 0;
}

static
Category categoryOf(Integer i)
{
	return (i % 2 == 0)? 'A' : 'B';
}

class DecisionTreeTest : public testing::Test
{
protected:
	typedef DecisionTree<Integer, Prime, Category> DecisionTree;

	vector<Integer> data;
	vector<Prime> features;

	DecisionTreeTest() :
	data(begin(nums), end(nums)),
	features(begin(primes), end(primes))
	{}
};

TEST_F(DecisionTreeTest, Construct)
{
	DecisionTree tree(
		data.begin(), data.end(),
		features.begin(), features.end(),
		primeFactorsOf,
		isPrimeFactor,
		categoryOf);

	EXPECT_EQ('A', tree.decide(2));
	EXPECT_EQ('A', tree.decide(5));
}
