#include "gtest/gtest.h"

#include <sstream>
#include "article.h"

using namespace std;

istream &operator >>(istream&, vector<Article>&);

TEST(Read_Articles, Read_Nothing)
{
	istringstream input;

	vector<Article> articles;
	input >> articles;

	EXPECT_TRUE(articles.empty());
}

TEST(Read_Articles, Read_1Article_1Word)
{
	istringstream input("1 1");

	vector<Article> articles;
	input >> articles;

	EXPECT_EQ(1, articles.size());
	EXPECT_EQ(1, articles[0].id());
}

TEST(Read_Articles, Read_Handles_Ending_Whitespace)
{
	istringstream input("1 1  \t \n");

	vector<Article> articles;
	input >> articles;

	EXPECT_EQ(1, articles.size());
	EXPECT_EQ(1, articles[0].id());
}

TEST(Read_Articles, Read_1Article_2Words)
{
	istringstream input(
		"1 1\n"
		"1 342\n");

	vector<Article> articles;
	input >> articles;

	EXPECT_EQ(1, articles.size());
	EXPECT_EQ(1, articles[0].id());
}

TEST(Read_Articles, Read_2Articles)
{
	istringstream input(
		"1 1\n"
		"1 342\n"
		"2 35\n");

	vector<Article> articles;
	input >> articles;

	EXPECT_EQ(2, articles.size());
	EXPECT_EQ(1, articles[0].id());
	EXPECT_EQ(2, articles[1].id());
}
