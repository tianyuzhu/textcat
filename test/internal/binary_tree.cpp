#include "gtest/gtest.h"

#include <utility>

#include "internal/binary_tree.hpp"

using namespace std;
using namespace internal;

typedef BinaryTree<int> Tree;

TEST(BinaryTree, Default_Constructible)
{
	Tree tree;
}

TEST(BinaryTree, Constructible_With_Reserve)
{
	Tree tree(7);
}

TEST(BinaryTree, Move_Constructible)
{
	Tree tree1;
	Tree tree2(move(tree1));
}

TEST(BinaryTree, Move_Assignable)
{
	Tree tree1;
	Tree tree2;
	tree2 = move(tree1);
}

TEST(BinaryTree, EmplaceRoot)
{
	Tree tree;
	auto root = tree.emplaceRoot(5);

	EXPECT_EQ(5, root.data());
}

TEST(BinaryTree, EmplaceLeft)
{
	Tree tree;
	auto root = tree.emplaceRoot(5);
	auto left = root.emplaceLeft(8);

	EXPECT_EQ(8, left.data());
}

TEST(BinaryTree, EmplaceRight)
{
	Tree tree;
	auto root = tree.emplaceRoot(5);
	auto right = root.emplaceRight(8);

	EXPECT_EQ(8, right.data());
}
