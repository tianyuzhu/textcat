#include "gtest/gtest.h"

#include <algorithm>
#include <utility>

#include "article.h"

using namespace std;

/// Utility function declarations ///

namespace {
	static
	vector<Word> const& words_of(Article const&);
}

/// Tests ///

TEST(Article, Default_Constructible)
{
	Article();
}

TEST(Article, Default_Id_Is_0)
{
	Article a;

	EXPECT_EQ(0, a.id());
}

TEST(Article, Template_Construction_With_IPositive_Id)
{
	vector<Word> words;
	Article a(28, words);

	ASSERT_EQ(28, a.id());
}

TEST(Article, Constructible_With_Copied_Words)
{
	vector<Word> words(3);
	Article a(1, words);

	ASSERT_EQ(3, words.size()) <<
		"Words should've been copied";
}

TEST(Article, Constructible_With_Moved_Words)
{
	vector<Word> words(3);
	Article a(1, move(words));

	ASSERT_EQ(0, words.size()) <<
		"Words should've been moved";
}

TEST(Article, Move_Constructible)
{
	vector<Word> words(3);
	Article a1(1, move(words));
	Article a2(move(a1));

	EXPECT_EQ(0, a1.id());
	EXPECT_EQ(1, a2.id());

	EXPECT_TRUE(words_of(a1).empty());
	EXPECT_EQ(3, words_of(a2).size());
}

TEST(Article, Move_Assignable)
{
	vector<Word> words(3);
	Article a1(1, move(words));
	Article a2;
	Article &result = (a2 = move(a1));

	EXPECT_EQ(&result, &a2);

	EXPECT_EQ(0, a1.id());
	EXPECT_EQ(1, a2.id());

	EXPECT_TRUE(words_of(a1).empty());
	EXPECT_EQ(3, words_of(a2).size());
}

TEST(Article, Move_Assignable_To_Self)
{
	vector<Word> words(3);
	Article a1(1, move(words));
	Article &result = (a1 = move(a1));

	EXPECT_EQ(&result, &a1);

	EXPECT_EQ(1, a1.id());

	EXPECT_EQ(3, words_of(a1).size());
}

TEST(Article, Doesnt_Contain_Any_Word)
{
	Word w(1);
	Article a(1, vector<Word>());

	EXPECT_FALSE(a.contains(w));
}

TEST(Article, Contains_Only_Word)
{
	Word w(1);
	vector<Word> words;
	words.push_back(w);

	Article a(1, move(words));

	EXPECT_TRUE(a.contains(w));
}

TEST(Article, Doesnt_Contain_Only_Word)
{
	Word w1(1), w2(2);
	vector<Word> words;
	words.push_back(w1);

	Article a(1, move(words));

	EXPECT_FALSE(a.contains(w2));
}

TEST(Article, Contains_Edge_Word)
{
	Word w1(1), w2(2);
	vector<Word> words;
	words.push_back(w1);
	words.push_back(w2);

	Article a(1, move(words));

	EXPECT_TRUE(a.contains(w2));
}

TEST(Article, Doesnt_Contain_Middle_Word)
{
	Word w1(1), w2(2), w3(3);
	vector<Word> words;
	words.push_back(w1);
	words.push_back(w3);

	Article a(1, move(words));

	EXPECT_FALSE(a.contains(w2));
}

TEST(Article, Contains_Middle_Word)
{
	Word w1(1), w2(2), w3(3);
	vector<Word> words;
	words.push_back(w1);
	words.push_back(w2);
	words.push_back(w3);

	Article a(1, move(words));

	EXPECT_TRUE(a.contains(w2));
}

TEST(Article, Doesnt_Contain_1_Of_8_Words)
{
	Word w6(6);
	Word ws[8] = {1, 2, 3, 4, 5, /*6,*/ 7, 8, 9};
	vector<Word> words;
	for (auto w : ws) words.push_back(w);

	Article a(1, move(words));

	EXPECT_FALSE(a.contains(w6));
}

TEST(Article, Contains_1_Of_8_Words)
{
	Word ws[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	vector<Word> words;
	for (auto w : ws) words.push_back(w);

	Article a(1, move(words));

	EXPECT_TRUE(a.contains(ws[6]));
}

/// Utility function definitions ///

namespace {
	struct X_Article_X
	{
		Article::Id id;
		vector<Word> words;
	};

	static
	X_Article_X const& expose(Article const&article)
	{
		return reinterpret_cast<X_Article_X const&>(article);
	}

	static
	vector<Word> const& words_of(Article const&article)
	{
		return expose(article).words;
	}
}
