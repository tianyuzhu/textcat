#include "gtest/gtest.h"

#include "article.h"

using namespace std;

TEST(ArticleLabels, Construct_With_Copied_Labels)
{
	vector<Label> labels;
	labels.emplace_back(1);
	labels.emplace_back(2);
	labels.emplace_back(3);

	ArticleLabels map(labels);

	ASSERT_EQ(3, labels.size());
}

TEST(ArticleLabels, Construct_With_Moved_Labels)
{
	vector<Label> labels;
	labels.emplace_back(1);
	labels.emplace_back(2);
	labels.emplace_back(3);

	ArticleLabels map(move(labels));

	ASSERT_TRUE(labels.empty());
}

TEST(ArticleLabels, LabelOf)
{
	vector<Label> labels;
	labels.emplace_back(1);
	labels.emplace_back(2);
	labels.emplace_back(3);
	vector<Word> words;

	ArticleLabels map(labels);

	Article::Id id = 0;
	for (auto &label : labels) {
		Article a(++id, words);
		ASSERT_EQ(label, map.labelOf(a)) <<
			"with id: " << id;
	}
}
