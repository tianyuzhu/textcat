#include "gtest/gtest.h"

#include <sstream>

#include "article.h"

using namespace std;

istream &operator >>(istream& input, vector<Label>& labels);

TEST(Read_Article_Labels, Read_Nothing)
{
	istringstream input;

	vector<Label> labels;
	input >> labels;

	EXPECT_TRUE(labels.empty());
}

TEST(Read_Article_Labels, Read_1Label)
{
	istringstream input("2");

	vector<Label> labels;
	input >> labels;

	EXPECT_EQ(1, labels.size());
	EXPECT_EQ(2, labels[0]);
}

TEST(Read_Article_Labels, Handles_Ending_Whitespace)
{
	istringstream input("1\n  \t");

	vector<Label> labels;
	input >> labels;

	EXPECT_EQ(1, labels.size());
	EXPECT_EQ(1, labels[0]);
}

TEST(Read_Article_Labels, Read_3Labels)
{
	istringstream input("1\n4\n2");

	vector<Label> labels;
	input >> labels;

	EXPECT_EQ(3, labels.size());
	EXPECT_EQ(1, labels[0]);
	EXPECT_EQ(4, labels[1]);
	EXPECT_EQ(2, labels[2]);
}
