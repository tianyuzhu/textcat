#include "gtest/gtest.h"

#include <sstream>
#include <string>

#include "article.h"

using namespace std;

istream &operator >>(istream& input, vector<string>& words);

TEST(Read_Words, Read_Nothing)
{
	istringstream input;

	vector<string> words;
	input >> words;

	EXPECT_TRUE(words.empty());
}

TEST(Read_Words, Read_1Word)
{
	istringstream input("foo");

	vector<string> words;
	input >> words;

	EXPECT_EQ(1, words.size());
	EXPECT_EQ(string("foo"), words[0]);
}

TEST(Read_Words, Handles_Ending_Whitespace)
{
	istringstream input("bar\n  \t");

	vector<string> words;
	input >> words;

	EXPECT_EQ(1, words.size());
	EXPECT_EQ(string("bar"), words[0]);
}

TEST(Read_Words, Read_3Words)
{
	istringstream input("bar\ntom\nfoo");

	vector<string> words;
	input >> words;

	EXPECT_EQ(3, words.size());
	EXPECT_EQ(string("bar"), words[0]);
	EXPECT_EQ(string("tom"), words[1]);
	EXPECT_EQ(string("foo"), words[2]);
}

ostream &operator<<(ostream &os, Word w)
{
	return os << w.id;
}
