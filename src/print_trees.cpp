#include <iostream>
#include <fstream>
#include <functional>
#include <utility>
#include <vector>

#include "article.h"
#include "decision_tree.h"
#include "io.h"
#include "decision_tree_io.h"

using namespace std;

template<typename T>
vector<T> read(char const *fileName)
{
	ifstream dataStream(fileName);
	vector<T> data;
	dataStream >> data;
	return data;
}

static
vector<Word> const&wordsInArticle(Article const&a)
{
	return a.words();
}

static
bool isWordInArticle(Article const&a, Word w)
{
	return a.contains(w);
}

static
vector<string> strings(read<string>("words.txt"));

ostream &operator <<(ostream &os, Word w)
{
	return os << strings[w.id];
}

int main()
{
	vector<Article> trainData(read<Article>("trainData.txt"));
	ArticleLabels trainLabels(read<Label>("trainLabel.txt"));

	vector<Word> words;
	{
		words.reserve(strings.size());
		Word w = 1;
		for (auto i = strings.begin(), end = strings.end();
			 i != end; ++i) {
			words.push_back(w++);
		}
	}

	// Tree evaluating on information gain
	DecisionTree<Article, Word, Label> tree1(
		trainData.begin(), trainData.end(),
		words.begin(), words.end(),
		wordsInArticle,
		isWordInArticle,
		bind(&ArticleLabels::labelOf,
			&trainLabels, placeholders::_1),
		Information_Gain);

	// Tree evaluating on information gain * size
	DecisionTree<Article, Word, Label> tree2(
		trainData.begin(), trainData.end(),
		words.begin(), words.end(),
		wordsInArticle,
		isWordInArticle,
		bind(&ArticleLabels::labelOf,
			&trainLabels, placeholders::_1),
		Information_Gain_Times_Size);

	for (int size = 1; size <= 10; ++size) {
		tree1.learnNode();
		tree2.learnNode();
	}

	cout << "Tree (Information Gain):\n" << tree1 << endl
		 << "Tree (Information Gain * Size):\n" << tree2;

	return 0;
}
