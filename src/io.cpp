#include <cassert>
#include <istream>
#include <ostream>
#include <utility>

#include "article.h"
#include "decision_tree.h"

#include "io.h"
#include "decision_tree_io.h"

using namespace std;

static
istream &operator >>(istream &input, Word &w)
{
	return input >> w.id;
}

static
vector<Word> read_article_words(Article::Id id, istream &in)
{
	vector<Word> words;
	words.reserve(300);

	// Read the words
	while (true) {
		Word word;
		in >> word;
		words.emplace_back(move(word));

		auto position = in.tellg(); // Remember position
		Article::Id next;
		// Continue if there's more
		if (in >> next && next == id) continue;
		in.seekg(position); // Recall position
		words.shrink_to_fit();
		return words; // return
	}
}

istream &operator >>(istream &in, vector<Article> &articles)
{
	Article::Id id;
	while (in >> id) {
		articles.emplace_back(id, read_article_words(id, in));
	}
	return in;
}

/**
 * A template function to read simple stream data where each
 * datum can be serialized with one >> operator.
 */
template<typename T, size_t Reserve>
static
istream &read_simple_data(istream &input, vector<T> &data)
{
	data.reserve(Reserve);

	T datum;
	while (input >> datum)
		data.emplace_back(move(datum));

	data.shrink_to_fit();

	return input;
}

istream &operator >>(istream& input, vector<Label>& labels)
{
	return read_simple_data<Label, (1<<11)>(input, labels);
}

istream &operator >>(istream &input, vector<string> &words)
{
	return read_simple_data<string, (1<<12)>(input, words);
}

/// DecisionTree operator << ///

typedef DecisionTree<Article, Word, Label> Tree;
typedef Tree::Node Node;

static
void printNode(ostream &output, const Node node, unsigned level)
{
	auto &decision = node.data();
	for (int i = 0; i < level; ++i)
		output << "| ";
	output << "+-";
	if (decision.is_leaf) {
		switch (decision.clazz) {
		case 1:
			output << "alt.atheism\n";
			break;
		case 2:
			output << "comp.graphics\n";
			break;
		}
	} else {
		output << '\"' << decision.feature << '\"'
		     << " (" << decision.value << ")\n";
		printNode(output, node.left(), level+1);
		printNode(output, node.right(), level+1);
	}
}

ostream &operator <<(ostream &output, Tree const&tree)
{
	printNode(output, tree.rootNode(), 0);
	output.flush();
	return output;
}
