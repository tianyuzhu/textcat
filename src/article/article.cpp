#include <algorithm>
#include <cassert>

#include "article.h"

using namespace std;

/// Helper assertions ///

#define assert_is_sorted(range) \
	assert(std::is_sorted( \
		std::begin(range), std::end(range)) && \
	#range " is not in sorted order!")

#define assert_valid_id(id) \
	assert((id) > 0 && \
	"Invalid id " #id " is not positive!")

/// Word implementation ///

Word::Word() :
id(0)
{}

Word::Word(unsigned int id) :
id(id)
{}

bool Word::operator ==(Word w) const
{
	return id == w.id;
}

bool Word::operator <(Word w) const
{
	return id < w.id;
}

Word Word::operator ++()
{
	return Word(++id);
}

Word Word::operator ++(int)
{
	return Word(id++);
}

typename hash<Word>::result_type
hash<Word>::operator ()(argument_type w) const
{
	return hash<unsigned int>()(w.id);
}

/// Article implementation ///

Article::Article() :
_id(),
_words()
{}

Article::Article(Article &&article) :
_id(article._id),
_words(move(article._words))
{
	article._id = 0;
}

Article &Article::operator =(Article &&article)
{
	if (&article == this) return *this;
	_id = article._id;
	_words = move(article._words);
	article._id = 0;
	return *this;
}

void Article::assert_validity() const
{
	assert_valid_id(_id);
	assert_is_sorted(_words);
}


bool Article::contains(Word word) const
{
	return binary_search(
		_words.begin(), _words.end(),
		word);
}

Article::Id Article::id() const
{
	return _id;
}

vector<Word> const& Article::words() const
{
	return _words;
}
