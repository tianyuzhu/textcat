#include <cassert>

#include "article.h"

#define assert_valid_id(map, id) \
	assert(0 < (id) && "id is negative!");\
	assert((id) <= (map).labels.size() && "id is too big!");

Label ArticleLabels::labelOf(Article const& article) const
{
	Article::Id id = article.id();
	assert_valid_id(*this, id);
	return labels[id-1];
}
