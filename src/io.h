#include <iosfwd>
#include <vector>

namespace std {	typedef basic_string<char> string; }

/**
 * Reads a vector of articles from an input stream.
 *
 * The data from the input stream must adhere to the
 * following requirements:
 * - plain-text (ASCII) encoding
 * - line-seperated
 * - each line has format: [article-id] [word-id]
 * - both article-id and word-id are positive integers
 * - lines are sorted first by ascending article-id
 * - lines are sorted second by ascending word-id
 *
 * @param input    The input stream to read from.
 * @param articles The vector of Articles to write to.
 * @return The given input stream after reading.
 */
std::istream &operator >>(
	std::istream& input,
	std::vector<Article>& articles);

/**
 * Reads a mapping of Articles to their Labels from
 * an input stream.
 *
 * The data from the input stream must be:
 * - plain-text (ASCII) encoding
 * - line-seperated
 * - each line contains a single field [label-id]
 * - an article with article-id I and label-id L has
 *   L on line I-1.
 * - label-id is a positive integer
 *
 * @param input  The input stream to read from.
 * @param labels The labels to append to.
 * @return The given input stream after reading.
 */
std::istream &operator >>(
	std::istream& input,
	std::vector<Label>& labels);

/**
 * Reads a mapping of Words to their actual string
 * representation.
 *
 * The data from the input stream must be:
 * - plain-text (ASCII) encoding
 * - line-seperated
 * - each line contains single field [string]
 * - each string is a single word containing no whitespace
 * - A word with word-id W and string representation S has
 *   S on line W-1
 *
 * @param input The input stream to read from.
 * @param words The vector of words to append to.
 * @return the given input stream after reading.
 */
std::istream &operator >>(
	std::istream &input,
	std::vector<std::string> &words);

/**
 * Prints a word.
 *
 * The implementation is left for the user.
 */
std::ostream &operator <<(std::ostream &os, Word w);
