#include "decision_tree.h"

/**
 * Prints a Decision tree to the specified ouput stream.
 *
 * @param output The output stream to write to.
 * @param tree   The decision tree to print.
 */
std::ostream &operator <<(
	std::ostream &output,
	DecisionTree<Article, Word, Label> const&tree);
