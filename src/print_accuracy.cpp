#include <iostream>
#include <fstream>
#include <functional>
#include <utility>
#include <vector>

#include "article.h"
#include "decision_tree.h"
#include "io.h"
#include "decision_tree_io.h"

using namespace std;

template<typename T>
vector<T> read(char const *fileName)
{
	ifstream dataStream(fileName);
	vector<T> data;
	dataStream >> data;
	return data;
}

static
vector<Word> const&wordsInArticle(Article const&a)
{
	return a.words();
}

static
bool isWordInArticle(Article const&a, Word w)
{
	return a.contains(w);
}

static
vector<string> strings(read<string>("words.txt"));

ostream &operator <<(ostream &os, Word w)
{
	return os << strings[w.id];
}

static
void test(
	const unsigned numNodes,
	DecisionTree<Article, Word, Label> const&tree,
	vector<Article> const&articles,
	ArticleLabels const&labels)
{
	unsigned total = 0;
	unsigned pass = 0;

	for (auto &article : articles) {
		Label actual = tree.decide(article);
		Label expected = labels.labelOf(article);
		++total;
		if (actual == expected) ++pass;
	}

	cout << numNodes
		 << "," << pass
		 << ',' << total
		 << endl;
}

int main()
{
	vector<Article> trainData(read<Article>("trainData.txt"));
	ArticleLabels trainLabels(read<Label>("trainLabel.txt"));
	vector<Article> testData(read<Article>("testData.txt"));
	ArticleLabels testLabels(read<Label>("testLabel.txt"));

	vector<Word> words;
	{
		words.reserve(strings.size());
		Word w = 1;
		for (auto i = strings.begin(), end = strings.end();
			 i != end; ++i) {
			words.push_back(w++);
		}
	}

	unsigned numNodes;

	cout << "Information Gain\n";

	DecisionTree<Article, Word, Label> tree1(
		trainData.begin(), trainData.end(),
		words.begin(), words.end(),
		wordsInArticle,
		isWordInArticle,
		bind(&ArticleLabels::labelOf,
			&trainLabels, placeholders::_1));

	numNodes = 1;
	while (true) {
		test(numNodes, tree1, testData, testLabels);
		if (numNodes == 100) break;
		tree1.learnNode();
		++numNodes;
	}

	cout << "\nInformation Gain * Size\n";

	DecisionTree<Article, Word, Label> tree2(
		trainData.begin(), trainData.end(),
		words.begin(), words.end(),
		wordsInArticle,
		isWordInArticle,
		bind(&ArticleLabels::labelOf,
			&trainLabels, placeholders::_1),
		Information_Gain_Times_Size);

	numNodes = 1;
	while (true) {
		test(numNodes, tree2, testData, testLabels);
		if (numNodes == 100) break;
		tree2.learnNode();
		++numNodes;
	}

	cout.flush();
	return 0;
}
