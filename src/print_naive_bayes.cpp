#include <fstream>
#include <functional>
#include <iostream>

#include "article.h"
#include "io.h"
#include "naive_bayes.hpp"

using namespace std;

template<typename T>
vector<T> read(char const *fileName)
{
	ifstream dataStream(fileName);
	vector<T> data;
	dataStream >> data;
	return data;
}

static
vector<Word> const&wordsInArticle(Article const&a)
{
	return a.words();
}

static
bool isWordInArticle(Article const&a, Word w)
{
	return a.contains(w);
}

static
vector<string> strings(read<string>("words.txt"));

ostream &operator <<(ostream &os, Word w)
{
	return os << strings[w.id];
}

static
void test(
	NaiveBayesNet<Article, Word, Label> const&net,
	vector<Article> const&articles,
	ArticleLabels const&labels)
{
	unsigned total = 0;
	unsigned pass = 0;

	for (auto &article : articles) {
		Label actual = net.decide(article);
		Label expected = labels.labelOf(article);
		++total;
		if (actual == expected) ++pass;
	}

	cout << pass
		 << ',' << total
		 << endl;
}

int main()
{
	vector<Article> trainData(read<Article>("trainData.txt"));
	ArticleLabels trainLabels(read<Label>("trainLabel.txt"));
	vector<Article> testData(read<Article>("testData.txt"));
	ArticleLabels testLabels(read<Label>("testLabel.txt"));
	vector<Label> labels;
	labels.push_back(1);
	labels.push_back(2);

	NaiveBayesNet<Article, Word, Label> net(
		trainData.begin(), trainData.end(),
		labels.begin(), labels.end(),
		wordsInArticle,
		isWordInArticle,
		bind(&ArticleLabels::labelOf,
			&trainLabels, placeholders::_1));

	test(net, testData, testLabels);
};
